import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import AppProvider from "./context/AppProvider";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./components/Login";
import Home from './components/Home';
import Members from './components/Members/Members';


ReactDOM.render(
  <AppProvider>
    <React.StrictMode>
      <BrowserRouter>
        <Switch>
          <Route path="/" exact={true} component={App} />
          <App/>
          <Route path="/login" component={Login} />
          <Route path="/home" component={Home} />
          <Route path='/members' component={Members}/>
        </Switch>
      </BrowserRouter>
    </React.StrictMode>
  </AppProvider>,
  document.getElementById("root")
);
