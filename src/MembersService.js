
export const URL_API = 'http://localhost:8080/api';

export function fetchMembersFromH2(success, failure, id) {
    fetch(`${URL_API}/families/2/members`)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err.message));
}
/*
export function addMemberToH2(success, failure, request) {
    console.log("chegou ao service")
    fetch(`${URL_API}/families/2/members`, request)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err.message));
}
*/

export async function addMemberToH2(success, failure, request) {

    try {

        const response = await fetch(`${URL_API}/families/2/members`, request);

        const responseCode = response.status;

        if (response.ok) {
            console.log("Service: response is ok, making json...")
            const responseData = await response.json();
            success(responseData);

        } else if (responseCode >= 400 && responseCode <= 499) {
            const responseData = await response.json(); /* response.text() */
            ;
            console.log("falhou no serviço",responseData);
            failure(responseData);

        } else {
            console.log("Service: response is broken.")
            throw new Error('partiu')
        }

    } catch (err) {
        console.log("Service: catch was called")
        failure(err);
    }

}

/*
export function fetchMembersFromH2(success, failure, id) {
    fetch(`${URL_API}/families/${id}/members`)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err.message));
}

*/
