import React, { useContext } from "react";
import AppContext from "../context/AppContext";
import "../Home.css";
import Menu from "./Menu";
import MemberForm from './Members/MemberForm';
import MemberTable from './Members/MemberTable';

function Home() {
  const { state, dispatch } = useContext(AppContext);
  const { user } = state;
  const { isLogged, data } = user;

  if (isLogged === true && data[2] === "systemManager") {
    return (
      <div>
        <Menu />
        <h1 className="welcome">Welcome {data[1]}!</h1>
        <div className="buttons"></div>
      </div>
    );
  }
  if (isLogged === true && data[2] === "admin") {
    console.log(data[2]);
    function handleClick() {
      return (
        <div>
            <MemberTable />
            <MemberForm />
        </div>
    );
    }
    return (
      <div>
        <Menu />
        <hr />
        <h1 className="welcome">Welcome {data[1]}!</h1>

        <button> click here </button>

        <div className="buttons">
          <button onClick={handleClick}>Family Members and Relations</button>
        </div>
      </div>
    );
  }
  if (isLogged === true && data[2] === "member") {
    return (
      <div>
        <Menu />
        <hr />
        <h1 className="welcome">Welcome {data[1]}!</h1>
        <div className="buttons">
          <button>My Profile</button>
        </div>
      </div>
    );
  } else {
    return alert("The email is not valid.");
  }
}

export default Home;
