import React from "react";
import AppContext from '../context/AppContext';
import { fetchUser } from '../context/Actions';
import { useState } from "react";
import "../Login.css";

const Login = () => {
  const { dispatch } = React.useContext(AppContext);

  const [useremail, setUserEmail] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    fetchUser(dispatch, useremail); 
         
  };

  return (
    <div>
      <link
        href="http://fonts.googleapis.com/css?family=Montserrat:400,700"
        rel="stylesheet"
        type="text/css"
      ></link>
      <div className="logo"></div>
      <div className="login-block">
        <form action="" onSubmit={handleSubmit}>
          <h1>Login</h1>

          <input
            type="email"
            className="mdl-textfield__input"
            placeholder="E-mail Address"
            value={useremail}
            onChange={(e) => setUserEmail(e.target.value)}
          />

          <input
            type="password"
            className="mdl-textfield__input"
            placeholder="Password"
          />

          <button type="submit">Submit</button>
        </form>
      </div>
    </div>
  );
};

export default Login;
