import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import AppContext from "../context/AppContext";
import React from "react";
import Home from "../components/Home";
import Login from "./Login";

const App = () => {
  const { state } = React.useContext(AppContext);
  const { isLogged } = state.user;
  console.log(isLogged);
  if (isLogged === true) {
    console.log(isLogged);
    return (    
     <Link to='/home' component={Home}/>
    );
  } else {
    console.log(isLogged);
    return (    
  <Link to='/login' component={Login}/>);
  }
};

export default App;
