import React from 'react';
import MemberForm from './MemberForm';
import MemberTable from './MemberTable';

function Members() {
    
    return (
        <div>
            <MemberTable />
            <MemberForm />
        </div>
    );
}
export default Members;