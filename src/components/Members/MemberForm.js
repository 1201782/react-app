import React, { useContext, useState } from 'react';
import {addMember} from  '../../context/Actions';
import AppContext from '../../context/AppContext';

function MemberForm() {
    const { state,dispatch } = useContext(AppContext);
    const [emailAddress, setEmail] = useState("");
    const [address, setAddress] = useState("");
    const [birthDate, setBirthdate] = useState("");
    const [familyID, setFamilyID] = useState("");
    const [name, setName] = useState("");
    const [vatNumber, setVat] = useState("");

    const handleOnClick = (emailAddress, address, birthDate,
        familyID, name, vatNumber) => {
            console.log("enviou submit do Form")
            addMember(dispatch, emailAddress, address, birthDate, familyID, name, vatNumber);

            setEmail("");
            setAddress("");
            setBirthdate("");
            setFamilyID("");
            setName("");
            setVat("");      
    }
    if(!state.members.on){
        return null;
    }
else{
    return (
        <form>
            <label>
                E-mail
                <input type="text" name="email" value={emailAddress} onChange={(event) => setEmail(event.target.value)} />
            </label>


            <label>
                Address
                <input type="text" name="address" value={address} onChange={(event) => setAddress(event.target.value)} />
            </label>


            <label>
                Birthdate
                <input type="text" name="birthDate" value={birthDate} onChange={(event) => setBirthdate(event.target.value)} />
            </label>

            <br></br>

            <label>
                FamilyID
                <input type="text" name="familyID" value={familyID} onChange={(event) => setFamilyID(event.target.value)} />
            </label>


            <label>
                Name
                <input type="text" name="name" value={name} onChange={(event) => setName(event.target.value)} />
            </label>


            <label>
                VAT
                <input type="text" name="vatNumber" value={vatNumber} onChange={(event) => setVat(event.target.value)} />
            </label>
            <br></br>
            <button type="button" value="Submit" onClick={() => handleOnClick(emailAddress, address, birthDate,
                familyID, name, vatNumber)}>SUBMIT</button>
        </form>
    );
}}
export default MemberForm;