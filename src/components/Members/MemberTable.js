import React, {useContext, useEffect} from 'react';

import AppContext from '../../context/AppContext';
import {fetchMembers} from '../../context/Actions';

import MaterialTable from "material-table";


function MemberTable() {
    const {state, dispatch} = useContext(AppContext);
    const {members} = state;
    const {loading, error, data} = members;

    useEffect(() => {
        fetchMembers(dispatch);
        console.log("USEEFFECT")
    }, []);

    const tableMembers = (<div>
        <h1>Material-Table Test for US 101</h1>
        <div style={{maxWidth: "100%"}}>
            <MaterialTable
                columns={[
                    {
                        title: "Email",
                        field: "emailAddress"
                    },
                    {
                        title: "Address",
                        field: "address"
                    },
                    {
                        title: "Birth Date",
                        field: "birthDate",

                    },
                    {
                        title: "FamilyID",
                        field: "familyID",

                    },
                    {
                        title: "Name",
                        field: "name",
                    },
                    {
                        title: "VAT",
                        field: "vatNumber",
                    }
                ]}
                data={data}
                title="Material-Table SWS US 101"
            />
        </div>
    </div>)


    if (!members.on) {
        return null;
    } else {
        if (loading === true) {
            console.log("chegou à table if loading")
            console.log(data)
            return (<h1>Loading ......</h1>);
        } else {
            if (error !== null) {
                return (
                    <>{tableMembers}
                        <h1>{error}</h1>
                    </>
                )

            } else {
                if (data.length > 0) {
                    return tableMembers;
                } else {
                    return (<h1>No data.... </h1>);
                }
            }
        }
    }
}

export default MemberTable;