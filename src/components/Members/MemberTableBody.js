import React, { useContext } from 'react';
import AppContext from '../../context/AppContext';


function MemberTableBody() {
    const { state } = useContext(AppContext);
    const { members } = state;
    const { data } = members;

    const rows = data.map((row, index) => {
        console.log("faz mapeamento no table body")
        return (
            <tr key={index}>
                <td>{row.emailAddress}</td>
                <td>{row.address}</td>
                <td>{row.birthDate}</td>
                <td>{row.familyID}</td>
                <td>{row.name}</td>
                <td>{row.vatNumber}</td>
            </tr>
        )
    });
    return (
        <tbody>{rows}</tbody>
    );
}

export default MemberTableBody;