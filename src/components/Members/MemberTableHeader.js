import React, { useContext } from 'react';
import AppContext from '../../context/AppContext';

/*class TableHeader extends Component {
    constructor(props) {
        super(props);
    }
*/

function MemberTableHeader() {
    //render() 
    const { membersHeaders } = useContext(AppContext);
    return (
        <thead>
            <tr>
                <th>{membersHeaders.email}</th>
                <th>{membersHeaders.address}</th>
                <th>{membersHeaders.birthdate}</th>
                <th>{membersHeaders.familyID}</th>
                <th>{membersHeaders.name}</th>
                <th>{membersHeaders.vat}</th>
            </tr>
        </thead>
    )
    //}
}
export default MemberTableHeader;