
import React, {useContext} from 'react';
import AppContext from "../../context/AppContext";
import {toggleMembers} from  '../../context/Actions';

function MembersToggler() {

    const {state,dispatch} = useContext(AppContext);

    const handleClick = () =>{
        toggleMembers(dispatch,state.members.on);
    }

    return (
        <div>
            <input type="button" onClick={handleClick} value="Members" />
        </div>
    )
}

export default MembersToggler;