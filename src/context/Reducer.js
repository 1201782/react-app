import {
  LOGIN,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
  LOGOUT,
  FETCH_MEMBERS_FAILURE,
  FETCH_MEMBERS_STARTED,
  FETCH_MEMBERS_SUCCESS,
  MEMBERS_TOGGLE_OFF,
  MEMBERS_TOGGLE_ON,
  ADD_MEMBER_FAILURE,
  ADD_MEMBER_STARTED,
  ADD_MEMBER_SUCCESS,
} from "./Actions";

function reducer(state, action) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        user: {
          isLogged: state.user.isLogged,
          error: null,
          data: [],
        },
      };
    case FETCH_USER_SUCCESS:
      console.log('success');
      return {
        ...state,
        user: {
          isLogged: true,
          error: null,
          data: action.payload.user,
        },
      };
    case FETCH_USER_FAILURE:
      console.log('falhou');

      return {
        ...state,
        user: {
          isLogged: false,
          error: action.payload.error,
          data: [],
        },
      };
    case LOGOUT:
      return {
        ...state,
        user: {
          isLogged: false,
          error: null,
          data: [],
        },
      };
    case FETCH_MEMBERS_STARTED:
      return {
        ...state,
        members: {
          on: state.members.on,
          loading: true,
          error: null,
          data: [],
        },
      };
    case FETCH_MEMBERS_SUCCESS:
      return {
        ...state,
        members: {
          on: state.members.on,
          loading: false,
          error: null,
          data: [...action.payload.data],
        },
      };
    case FETCH_MEMBERS_FAILURE:
      return {
        ...state,
        members: {
          on: state.members.on,
          loading: true,
          error: null,
          data: [],
        },
      };
    case ADD_MEMBER_STARTED:
      return {
        ...state,
        members: {
          on: state.members.on,
          loading: true,
          error: null,
          data: [...state.members.data],
        },
      };
    case ADD_MEMBER_SUCCESS:
      console.log("chegou ao reducer success");
      return {
        ...state,
        members: {
          on: state.members.on,
          loading: false,
          error: null,
          data: [...state.members.data, action.payload.data],
        },
      };
    case ADD_MEMBER_FAILURE:
      console.log("chegou ao reducer failure");
      return {
        ...state,
        members: {
          on: state.members.on,
          loading: false,
          error: action.payload.error,
          data: state.members.data,
        },
      };
    case MEMBERS_TOGGLE_ON:
      return {
        ...state,
        members: {
          on: true,
          loading: false,
          error: null,
          data: [...state.members.data],
        },
        categories: {
          on: false,
          loading: false,
          error: null,
          data: [...state.categories.data],
        },
        families: {
          on: false,
          loading: false,
          error: null,
          data: [...state.families.data],
        },
        relationships: {
          on: false,
          relSubmitSpinner: false,
          loading: false,
          error: null,
          data: [...state.relationships.data],
        },
        info: {
          on: false,
          loading: false,
          error: null,
          data: [...state.info.data],
        },
      };

    case MEMBERS_TOGGLE_OFF:
      return {
        ...state,
        members: {
          on: false,
          loading: false,
          error: null,
          data: [...state.members.data],
        },
      };
    default:
      return state;
  }
}

export default reducer;
