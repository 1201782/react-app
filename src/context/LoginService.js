export const URL_API = 'http://localhost:8080/api';

export function fetchUserFromWS(email, success, failure){
    fetch(`${URL_API}/login`, {method: 'POST',  headers: {
      "Content-Type": "application/json",
  }, body: JSON.stringify({ email })})
      .then(res =>  res.json())
      .then(res =>success(res))
      .catch(err => failure(err.message))
      ;
}