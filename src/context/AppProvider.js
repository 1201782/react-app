import React, {useReducer} from 'react';
import PropTypes from "prop-types";
import {Provider} from './AppContext';
import reducer from './Reducer';

const initialState = {
  user: {
    isLogged: false,
    error: null,
    data: [],
  },
  members: {
    on: false,
    loading: true,
    error: null,
    data: [],
},
};


const userHeaders = {
  emailAddress: "Email",
  name: "Name",
  vat: "VAT Number",
  birthDate: "Birth Date",
};

const membersHeaders = {
  email: "E-mail",
  address: "Address",
  birthdate: "Birthdate",
  familyID: "Family",
  name: "Name",
  vat: "VAT",
};

const AppProvider = (props) =>{
    const [state, dispatch] = useReducer(reducer, initialState);
    return (
      <Provider value={{
          state,
          userHeaders,
          membersHeaders,
          dispatch}}>
          {props.children}
      </Provider>
  );
};

AppProvider.propTypes = {
  children: PropTypes.node,
};    
  
export default AppProvider;
