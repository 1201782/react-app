import { fetchUserFromWS } from './LoginService'
import {fetchMembersFromH2, addMemberToH2} from '../MembersService'
import Members from '../components/Members/Members';

export const LOGIN = 'LOGIN';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';

export const LOGOUT = 'LOGOUT';


export function fetchUser(dispatch, useremail){
  dispatch(login());
  fetchUserFromWS(useremail, (res) => dispatch(fetchUserSuccess(res)), (err) =>dispatch(fetchUserFailure(err.message)));
}

export function login() {
  return {
    type: LOGIN,    
  }
}

export function fetchUserSuccess(user) {
  return {
    type: FETCH_USER_SUCCESS,
    payload:{
      user: user
    }
  }
}

export function fetchUserFailure(message) {
  return {
    type: FETCH_USER_FAILURE,
    payload: {
      error: message
    }
  }
}

export function logout() {
  return {
    type: LOGOUT,
  }
}

export const FETCH_MEMBERS_STARTED = 'FETCH_MEMBERS_STARTED';
export const FETCH_MEMBERS_SUCCESS = 'FETCH_MEMBERS_SUCCESS';
export const FETCH_MEMBERS_FAILURE = 'FETCH_MEMBERS_FAILURE';


export function fetchMembers(dispatch) {
    dispatch(fetchMembersStarted());
    fetchMembersFromH2((res) => dispatch(fetchMembersSuccess(res)), (err) => dispatch(fetchMemberFailure(err.message)));
}

export function fetchMembersStarted() {
    return {
        type: FETCH_MEMBERS_STARTED
    }
}

export function fetchMembersSuccess(members) {
    return {
        type: FETCH_MEMBERS_SUCCESS,
        payload: {
            data:
                [...members]
        }
    }
}

export function fetchMemberFailure(message) {
    return {
        type: FETCH_MEMBERS_FAILURE,
        payload: {
            error: message
        }
    }
}
//----------------Members--------------------------

export const ADD_MEMBER_STARTED = 'ADD_MEMBER_STARTED';
export const ADD_MEMBER_SUCCESS = 'ADD_MEMBER_SUCCESS';
export const ADD_MEMBER_FAILURE = 'ADD_MEMBER_FAILURE';

export function addMember(dispatch, emailAddress, address, birthDate, familyID, name, vatNumber) {
    dispatch(addMemberStarted())
    console.log("chegou ao addMember -actions")

    const data = {
        emailAddress: emailAddress,
        address: address,
        birthDate: birthDate,
        familyID: familyID,
        name: name,
        vatNumber: vatNumber,
    };

    const req = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data)
    };
    console.log("envia dados depois do stringify");

    addMemberToH2((res) => dispatch(addMemberSuccess(res)),
        (err) => dispatch(addMemberFailure(err.message)), req);
}

export function addMemberStarted() {
    console.log("chegou às actions - addMemberStarted")
    return {
        type: ADD_MEMBER_STARTED
    }
}

export function addMemberSuccess(member) {
    console.log("chegou às actions - addMemberSuccess")
    return {
        type: ADD_MEMBER_SUCCESS,
        payload: {
            data: member
        }
    }
}

export function addMemberFailure(message) {
    console.log("chegou às actions - addMemberFailure")
    return {
        type: ADD_MEMBER_FAILURE,
        payload: {
            error: message
        }
    }
}

export function toggleMembers(dispatch,bool){
    if(bool){
        dispatch(toggleMembersOff())
    }
    else{
        dispatch(toggleMembersOn())
    }
}

export const MEMBERS_TOGGLE_ON = "MEMBERS_TOGGLE_ON";
export const MEMBERS_TOGGLE_OFF = "MEMBERS_TOGGLE_OFF";


export function toggleMembersOn(){
    return {
        type : MEMBERS_TOGGLE_ON
    }
}

export function toggleMembersOff(){
    return {
        type : MEMBERS_TOGGLE_OFF
    }
}




